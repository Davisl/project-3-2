package com.example.lelandtipcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SeekBar seekBar;
    private TextView textView;
    private EditText total;
    private Button button;
    private TextView tipTotal;

    double percent;
    double bill;
    double tip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        seekBar = (SeekBar) findViewById(R.id.perSeek);
        textView = (TextView) findViewById(R.id.perView);
        total = (EditText) findViewById(R.id.cost);
        button = (Button) findViewById(R.id.tally);
        tipTotal = (TextView) findViewById(R.id.end);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {

            public void onStopTrackingTouch(SeekBar bar)
            {
                int value = bar.getProgress();
            }

            public void onStartTrackingTouch(SeekBar bar)
            {

            }

            public void onProgressChanged(SeekBar bar,
                                          int paramInt, boolean paramBoolean)
            {
                textView.setText("" + paramInt + "%");
                percent = bar.getProgress();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String value2 = total.getText().toString();
                bill = Integer.parseInt(value2);
                tip = percent/bill * 100;
                String stringDouble = Double.toString(tip);
                tipTotal.setText("$" + stringDouble);
            }
        });
    }
}

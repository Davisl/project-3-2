package com.example.lelanddavisapp;

import android.os.Bundle;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnSubmit;
    private TextView txtView1;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        txtView1 = (TextView) findViewById(R.id.txtView1);
        editText = (EditText) findViewById(R.id.editText);

        btnSubmit.setText(R.string.Button);
        btnSubmit.setTextColor(Color.RED);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText;
                inputText = editText.getText().toString();
                txtView1.setText(inputText);
            }

            });
        }

    }
    /*
    public void showText(View view) {



    }*/

